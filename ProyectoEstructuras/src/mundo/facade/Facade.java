package mundo.facade;

import mundo.contenedora.Nodo;
import mundo.contenedora.Table;
import mundo.dao.SongDAO;
import mundo.db.Conexion;
import mundo.contenedora.Message;

public class Facade implements IFacade
{
	private SongDAO SongsDao;
	
	public Facade()
	{
		SongsDao = new SongDAO();
	}

	@Override
	public void insert(Conexion con, Message message) 
	{
		if(message.getTable().equals(Table.SONGS))
		{
			SongsDao.insert(con, message);
		}
	}

	@Override
	public void delete(Conexion con, Message message) 
	{
		if(message.getTable().equals(Table.SONGS))
		{
			SongsDao.delete(con, message);
		}
	}
	
	
	@Override
	public void update(Conexion con, Message message) 
	{	
		if(message.getTable().equals(Table.SONGS))
		{
			SongsDao.update(con, message);
		}
	}

	
	@Override
	public Nodo consultAll(Conexion con, Message message) {

		Nodo nvo = new Nodo<>();
		{
			if(message.getTable().equals(Table.SONGS))
			{
				nvo = SongsDao.consultAll(con, message);
			}
		}
		return nvo;
	}

	
	@Override
	public Nodo consult(Conexion con, Message message) 
	{
		Nodo nvo = new Nodo<>();
		{
			if(message.getTable().equals(Table.SONGS))
			{
				nvo = SongsDao.consult(con, message);
			}
		}
		return nvo;
	}

}
