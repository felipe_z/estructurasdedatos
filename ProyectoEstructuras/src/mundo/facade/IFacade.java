package mundo.facade;

import mundo.contenedora.Nodo;
import mundo.db.Conexion;
import mundo.contenedora.Message;

public interface IFacade 
{
	public void insert(Conexion con, Message messa);
	public void delete(Conexion con, Message messa);
	public void update(Conexion con, Message messa);
	public Nodo consultAll(Conexion con, Message messa);
	public Nodo consult(Conexion con, Message messa);
}
