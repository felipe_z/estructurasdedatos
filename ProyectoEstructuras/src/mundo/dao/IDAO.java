package mundo.dao;

import mundo.db.Conexion;
import mundo.contenedora.Message;
import mundo.contenedora.Nodo;

public interface IDAO 
{
	public void insert(Conexion con, Message message);
	public void delete(Conexion con, Message message);
	public void update(Conexion con, Message message);
	public Nodo consultAll(Conexion con, Message message);
	public Nodo consult(Conexion con, Message message);
}
