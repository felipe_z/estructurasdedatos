package mundo.servidor;

import java.io.IOException;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import mundo.contenedora.Message;
import mundo.contenedora.Nodo;

public class User 
{

	public final static int PUERTO = 1425;
	
	private String host = "localhost";
	
	private Socket client;
	
	public User()
	{
		
	}
	
	public Nodo sendMessage(Message messa) throws UnknownHostException, IOException, ClassNotFoundException 
	{
		
		client = new Socket(host, PUERTO);
		System.out.println("Connected User!!");
		
		ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
	
		oos.writeObject(messa);
		
		ObjectInputStream ois = new ObjectInputStream(client.getInputStream());
		
		Nodo nodo = (Nodo) ois.readObject();
		
		return nodo;
		
	}
}
