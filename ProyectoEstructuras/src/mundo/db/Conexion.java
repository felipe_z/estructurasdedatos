
package mundo.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author crisVargas
 *
 */
public class Conexion {
	private Connection  con;
	private Statement st;
	private ResultSet rs;

	public Conexion(String url, String user, String password){
		try {
			con= DriverManager.getConnection(url, user, password);
			System.out.println("Successful Connection");
		} catch (SQLException e) {
			System.out.println("Error connecting: "+e.getMessage());
		}
	}
	
	public ResultSet ejecuteConsult(String instruccionSql){
		rs = null;
		try {
			st = con.createStatement();
			rs = st.executeQuery(instruccionSql);
			System.out.println("Consult Successful");
		} catch (SQLException e) {
			System.out.println("Query Error: "+e.getMessage());
		}
		return rs;
	}
	
	public boolean ejecuteUpdate(String instruccionSql){
		int filas=0;
		try {
			st = con.createStatement();
			filas = st.executeUpdate(instruccionSql);
			System.out.println("Consult successful");
			return true;
		} catch (SQLException e) {
			System.out.println("error in the query: "+e.getMessage());
			return false;
		}
	}

	
    public boolean IsClosed() {
        try {
            return con.isClosed();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

}
