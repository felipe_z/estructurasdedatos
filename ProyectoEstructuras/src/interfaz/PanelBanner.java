package interfaz;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PanelBanner extends JPanel
{
	// -----------------------------------------------------
	// Atributos
	// -----------------------------------------------------
		
	/** Etiqueta para mostrar la imagen del banner.	 */
		private JLabel labImage;
		
	// -----------------------------------------------------
	// Constructor
	// -----------------------------------------------------
		
    /** Crea el panel del banner. */
		
	public PanelBanner ( )
	{
		
		// Se crea el objeto del panel
		labImage = new JLabel(new ImageIcon("data/Pimage.jpg"));
			
		// Agregar el objeto al panel
		add( labImage );
	}
	
}
