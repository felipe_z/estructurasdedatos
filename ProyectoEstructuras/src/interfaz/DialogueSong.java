package interfaz;

import java.awt.BorderLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;

public class DialogueSong extends JDialog {

	
	
	private PanelBannerSong banner;
	
	private PanelInformationSong panelInfo;
	
	public DialogueSong()
	{
		setTitle( "Song" );
        setSize( 570, 850 );
        setResizable( false );
        setLocationRelativeTo(null);
        setModal(true);
        
        // Distribuidor grafico en los bordes
        setLayout( new BorderLayout( ) );        
        
        banner = new PanelBannerSong();
        add( banner, BorderLayout.NORTH );
        
        panelInfo = new PanelInformationSong( this );
        add(panelInfo, BorderLayout.CENTER);
	}

}
