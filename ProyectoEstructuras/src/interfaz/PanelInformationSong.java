package interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import mundo.contenedora.Funcion;
import mundo.contenedora.Message;
import mundo.contenedora.Nodo;
import mundo.contenedora.Table;
import mundo.servidor.User;

public class PanelInformationSong extends JPanel implements ActionListener {

	
	public final static String INSERT = "Insert";
	
	public final static String DELETE = "Delete";
	
	public final static String MODIFY = "Modify";
	
	public final static String CONSULT = "Consult";
	
	public final static String CONSULT_ID = "Consult por ID";
	
	public final static String START = "Start";
	
	public final static String CLEAR = "Clear";
		
    private DialogueSong main;
	
	private JLabel labID;
	
	private JLabel labNameSong;
	
	private JLabel labNameInterpreter;
	
	private JLabel labGenre;
	
	private JLabel labDuration;
	
	private JTextField txtID;
	
	private JTextField txtNameSong;
	
	private JTextField txtNameInterpreter;
	
	private JTextField txtGenre;
	
	private JTextField txtDuration;
	
	private JTextArea txtResults;
	
	private JButton butAdd;
	
	private JButton butDelete;
	
	private JButton butModify;
	
	private JButton butConsult;
	
	private JButton butConsultID;

	private JButton butStart;
	
	private JButton butClear;
	
	private JScrollPane scroll;
	
	private Funcion funcion;
	private Table table = Table.SONGS;

	
	public PanelInformationSong( DialogueSong ia )
	{
		main = ia;
		
		TitledBorder edge = BorderFactory.createTitledBorder("Information of the interpreter");
		edge.setTitleColor( Color.BLACK );
		setBorder( edge );
		
		// Distribuidor grafico en los bordes
		setLayout( new BorderLayout( ) );
		
		JPanel panelInfo = new JPanel();
		
		panelInfo.setLayout(new GridLayout(3, 4));
		panelInfo.setPreferredSize( new Dimension( 0, 80 ) );
		
		labID = new JLabel(" ID: ");
		labNameSong = new JLabel(" NameSong: ");
		labNameInterpreter = new JLabel(" NameInterpreter: ");
		labGenre = new JLabel(" Genre: ");
		labDuration = new JLabel(" Duration: ");
		
		txtID = new JTextField();
		txtID.setEditable(true);
		
		txtNameSong = new JTextField();
		txtNameSong.setEditable(true);
		
		txtNameInterpreter = new JTextField();
		txtNameInterpreter.setEditable(true);
		
		txtGenre = new JTextField();
		txtGenre.setEditable(true);
		
		txtDuration = new JTextField();
		txtDuration.setEditable(true);
		
		panelInfo.add(labID);
		panelInfo.add(txtID);
		panelInfo.add(labNameSong);
		panelInfo.add(txtNameInterpreter);
		panelInfo.add(labNameInterpreter);
		panelInfo.add(txtNameSong);
		panelInfo.add(labGenre);
		panelInfo.add(txtGenre);
		panelInfo.add(labDuration);
		panelInfo.add(txtDuration);
	
		add(panelInfo, BorderLayout.NORTH);
		
		//Panel de resultados
		JPanel panelResults = new JPanel();
		TitledBorder edge1 = BorderFactory.createTitledBorder("Results");
		edge1.setTitleColor( Color.black );
		panelResults.setLayout(new BorderLayout());
		panelResults.setBorder( edge1 );
		
		txtResults = new JTextArea();
		txtResults.setBackground(Color.WHITE);
		txtResults.setEditable(false);
		scroll = new JScrollPane(txtResults);
		
		panelResults.add(scroll,BorderLayout.CENTER);
		
		add(panelResults);
		
		//Panel de Navegacion
		JPanel panelNavegacion= new JPanel();
				
		//Adiciona un marco con titulo
		TitledBorder edge2 = BorderFactory.createTitledBorder("Navegation");
		edge2.setTitleColor( Color.black );
		panelNavegacion.setBorder( edge2 );
		
		//Establece las dimensiones del panel
		panelNavegacion.setPreferredSize( new Dimension( 0,90 ) );
		
		butAdd = new JButton("Insert");
		butAdd.setActionCommand(INSERT);
		butAdd.addActionListener(this);
		
		butDelete = new JButton("Delete");
		butDelete.setActionCommand(DELETE);
		butDelete.addActionListener(this);
		
		butModify = new JButton("Modify");
		butModify.setActionCommand(MODIFY);
		butModify.addActionListener(this);
		
		butConsult = new JButton("Consult");
		butConsult.setActionCommand(CONSULT);
		butConsult.addActionListener(this);
		
		butConsultID = new JButton("Consult por ID");
		butConsultID.setActionCommand(CONSULT_ID);
		butConsultID.addActionListener(this);
		 
		butStart = new JButton("Start");
		butStart.setActionCommand(START);
		butStart.addActionListener(this);
		
		butClear = new JButton("Clear");
		butClear.setActionCommand(CLEAR);
		butClear.addActionListener(this);
		
				
		panelNavegacion.add(butAdd);
		panelNavegacion.add(butDelete);
		panelNavegacion.add(butModify);
		panelNavegacion.add(butConsult);
		panelNavegacion.add(butConsultID);
		panelNavegacion.add(butStart);
		panelNavegacion.add(butClear);
		
		add(panelNavegacion, BorderLayout.SOUTH);
	}


	@Override
	public void actionPerformed(ActionEvent event) 
	{
		String execution = event.getActionCommand();
		User user = new User();
		
		if(INSERT.equals(execution))
		{
			int menssage = JOptionPane.showConfirmDialog(null, "It wants to add a register?", "ADD", JOptionPane.YES_NO_OPTION);
			if(menssage == JOptionPane.YES_OPTION)
			{
				JOptionPane.showMessageDialog(null, "Please enter the data of the song you want to add");
				funcion = Funcion.INSERT;
			}		
		}
		else if(DELETE.equals(execution))
		{

			int menssage = JOptionPane.showConfirmDialog(null, "It wants to delete a register?", "DELETE", JOptionPane.YES_NO_OPTION);
			if(menssage == JOptionPane.YES_OPTION)
			{
				JOptionPane.showMessageDialog(null, "Please enter the ID of the song you want to delete");

				funcion = Funcion.DELETE;
			}		
		}
		
		else if(MODIFY.equals(execution))
		{
			int menssage = JOptionPane.showConfirmDialog(null, "It wants to modify a register?", "MODIFY", JOptionPane.YES_NO_OPTION);
			if(menssage == JOptionPane.YES_OPTION)
			{
				JOptionPane.showMessageDialog(null, "Please enter the data of the song you want to modify");

				funcion = Funcion.UPDATE;
			}
		}	
		
		else if(CONSULT.equals(execution))
		{
			int menssage = JOptionPane.showConfirmDialog(null, "You want to consult for all elements?", "CONSULT?",  JOptionPane.YES_NO_OPTION);
			if(menssage == JOptionPane.YES_OPTION)
			{
				

				funcion = Funcion.SELECT;
			}
			else
			{
				JOptionPane.showMessageDialog(null, "All existing elements will be consulted");

				funcion = Funcion.SELECT;
			}
		}
		
		else if(CONSULT_ID.equals(execution))
		{
			int menssage = JOptionPane.showConfirmDialog(null, "You want to check your ID for a song?", "CONSULT ID?",  JOptionPane.YES_NO_OPTION);
			if(menssage == JOptionPane.YES_OPTION)
			{
				JOptionPane.showMessageDialog(null, "Please enter the ID of the song you want to consult");

				funcion = Funcion.SELECT_ID;
			}
			else
			{
				JOptionPane.showMessageDialog(null, "All existing id elements will be consulted");

				funcion = Funcion.SELECT_ID;
			}
		}
		
		
		else if(START.equals(execution)) 
		{
			try {
				Message menssage = newMessage();
				
				if(funcion.equals(Funcion.SELECT) || funcion.equals(Funcion.SELECT_ID))
				{
					Nodo nodo = user.sendMessage(menssage);
					
					if(nodo != null)
					{
						String resultado = "";
						
						while(nodo != null)
						{
							resultado +=  nodo.getInformation().toString() + "\n";
							nodo = nodo.getNext();
						}
						
						txtResults.setText(resultado);							
					}
				}
				else
				{
					user.sendMessage(menssage);
					JOptionPane.showMessageDialog(null, "Operation has been performed correctly.");
				}
				
				
			} 
			catch (Exception e) {
				JOptionPane.showMessageDialog(null, "There was an error: "+ "\n" + e.getMessage());
			}
		}
		else if (CLEAR.equals(execution))
		{
			txtID.setText("");
			txtNameSong.setText("");
			txtNameInterpreter.setText("");
			txtGenre.setText("");
			txtDuration.setText("");
			txtResults.setText("");	
		}
	}
		
	public Message newMessage()
	{
		Message menssage = new Message();
		
		if(funcion.equals(Funcion.SELECT))
			{
				
				menssage.funcionCanciones(funcion, 1, " ", " ", 1, "");
			}
			else
			{
				int id = Integer.parseInt(txtID.getText());
				String nameSong = txtNameSong.getText();
				String nameInterpreter = txtNameInterpreter.getText();
				double duration = txtDuration.getText().isEmpty() ? 0 : Double.parseDouble(txtDuration.getText());
				String genre = txtGenre.getText();
			
				
				menssage.funcionCanciones(funcion, id, nameSong, nameInterpreter, duration, genre);
			}
			
		
		return menssage;
		
	}
		
	
}
